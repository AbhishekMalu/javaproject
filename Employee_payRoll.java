import java.util.*;

abstract class Employee{
    private int id;
    private String name;
    public Employee(int id, String name)
    {
        this.id = id;
        this.name = name;
    }

    public int getId(){
        return this.id;
    }

    public String getName()
    {
        return this.name;
    }

    abstract public double calculateSalary();

    @Override
    public String toString()
    {
        return "Employee [name: "+name+", id: "+id+", salary: "+calculateSalary()+"]";
    }
}

class FullTimeEmployee extends Employee{
    private float constantSalary;
    public FullTimeEmployee(int id, String name, float constantSalary) {
        super(id, name);
        this.constantSalary = constantSalary;
    }

    @Override
    public double calculateSalary() {
        return constantSalary;
    }
}

class PartTimeEmployee extends Employee{

    private float hour;
    private float salaryPerHrs;

    public PartTimeEmployee(int id, String name, float hour, float salaryPerHrs) {
        super(id, name);
        this.hour = hour;
        this.salaryPerHrs = salaryPerHrs;
    }

    @Override
    public double calculateSalary() {
        return hour*salaryPerHrs;
    }

}

class PayRoll{
    private List<Employee> employeeList;

    public PayRoll()
    {
        employeeList = new ArrayList<>();
    }

    //  Add employee in employeeList
    public void addEmployee(Employee emp)
    {
        employeeList.add(emp);
    }

    // Remove employee from employeelist
    public void removeEmployee(int id)
    {
        for(Employee emp: employeeList)
        {
            if(emp.getId()==id)
            {
                employeeList.remove(emp);
                break;
            }
        }
    }

    // Display all employee
    public void displayAllEmployee()
    {
        for(Employee emp: employeeList)
        {
            System.out.println(emp);
        }
    }

}

public class Employee_payRoll{
    public static void main(String[] args)
    {
        System.out.println("This is structure of our project!!");
        FullTimeEmployee emp1 = new FullTimeEmployee(1111,"Abhishek", 60000);
        PartTimeEmployee emp2 = new PartTimeEmployee(1021,"Anjali",5,250);

        PayRoll payRolSystem = new PayRoll();
        payRolSystem.addEmployee(emp1);
        payRolSystem.addEmployee(emp2);

        payRolSystem.displayAllEmployee();
    }
}