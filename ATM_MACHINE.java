import java.util.*;

class ATM{
    private int atm_pin;
    private float balance;
    public ATM(int pin, float bal)
    {
        this.atm_pin = pin;
        this.balance = bal;
        System.out.println("Welcome to the SBI Bank!!!");
        this.checkpin();
    }

    public void checkpin()
    {
        System.out.print("Enter your PIN Number: ");
        Scanner sc = new Scanner(System.in);
        int pin = sc.nextInt();
        if(pin==this.atm_pin)
        {
            this.manu();
        }
        else{
            System.out.println("Enter the valid PIN number...");
            this.checkpin();
        }
    }
    
    public void manu()
    {
        System.out.println("Enter Your Choice: ");
        System.out.println("1.Mini-Statement\t2.Deposite Money\n3.Withdrwal Money\t4.Exit");
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        switch (n) {
            case 1:
                min_statement();
                break;
            case 2:
                deposite_money();
                break;
            case 3:
                withdrawl_money();
                break;
            case 4:
                exit();
                break;
        }
    }

    public void min_statement()
    {
        System.out.println("Your Available Balance is "+this.balance+".");
        this.manu();
    }
    public void deposite_money()
    {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter Amount to Deposite: ");
        float money = sc.nextFloat();
        this.balance += money;
        this.manu();
    }
    public void withdrawl_money()
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Amount to Withdraw: ");
        float money = sc.nextFloat();
        if(money>this.balance){
            System.out.println("InSufficient Amount!!!");
        }
        else{
            this.balance -= money;
            System.out.println("Money Withdraw Successful!!");
        }
        this.manu();
    }
    public void exit()
    {
        return;
    }

}

public class ATM_MACHINE {
    public static void main(String[] args)
    {
        System.out.println("Atm-Machine");
        ATM user1 = new ATM(2003, 12005);
    }
}